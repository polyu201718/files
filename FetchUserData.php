<?php
	require_once('mysql.inc.php'); 
    
      
    @$user_id = $_POST["user_id"];
  
    
    $statement = mysqli_prepare($dbc, "SELECT * FROM DM_USER WHERE USER_ID = ?");
    mysqli_stmt_bind_param($statement, "i", $user_id);
    mysqli_stmt_execute($statement);
    
    mysqli_stmt_store_result($statement);
    mysqli_stmt_bind_result($statement, $id, $user_id, $name, $birthday, $sex, $smoker, $injection, $morning_start, $morning_end, $breakfast_start,
	$breakfast_end, $lunch_start, $lunch_end, $dinner_start, $dinner_end, $bed_start, $bed_end, $height_unit, $weight_unit, $waist_unit);
    
    $user = array();
    

    while(mysqli_stmt_fetch($statement)){
		$user["user_id"] = $user_id;
        $user["name"] = $name;
        $user["birthday"] = $birthday;
        $user["sex"] = $sex;
        $user["smoker"] = $smoker;
		$user["injection"] = $injection;
		$user["morning_start"] = $morning_start;
		$user["morning_end"] = $morning_end;
		$user["breakfast_start"] = $breakfast_start;
		$user["breakfast_end"] = $breakfast_end;
		$user["lunch_start"] = $lunch_start;
		$user["lunch_end"] = $lunch_end;
		$user["dinner_start"] = $dinner_start;
		$user["dinner_end"] = $dinner_end;
		$user["bed_start"] = $bed_start;
		$user["bed_end"] = $bed_end;
		$user["height_unit"] = $height_unit;
		$user["weight_unit"] = $weight_unit;
		$user["waist_unit"] = $waist_unit;
		
    }  
 
    	echo json_encode($user);
		mysqli_stmt_close($statement); 
		closeConnectDB();
	 
	

?>
