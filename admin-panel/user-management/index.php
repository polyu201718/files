<?php
  require_once(dirname(__FILE__, 2).'/api/controller/auth.php');
  $authContoller = new authContoller();

  if (!$authContoller->isLogedin()) {
    $path = "https://".$_SERVER['HTTP_HOST']."/dma/prediabetes/admin-panel/login/?redirect=".urlencode("user-management");
    header("Location: $path");
    exit();
  }
  $authContoller->activitynotify($_SESSION['uid']);
  print file_get_contents("index.page");

?>
