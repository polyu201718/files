<?php
require_once(dirname(__FILE__, 3).'/mysql.inc.php');

if ($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($_POST['event'])) {
    header('HTTP/1.0 404 Not Found');
  //print var_dump($_POST);
  exit();
}

$request_event = trim($_POST['event']);
$request_method = (isset($_POST['method']))?trim($_POST['method']):"";
$request_data = (isset($_POST['data']))?json_decode(trim(urldecode($_POST['data']))):"";

//User Id Verify
//$sql = 'SELECT ID FROM DM_USER WHERE USER_ID = '.$request_user_id;
//$result = select_result($sql);

//if (get_rowCount($result) <= 0) {
//    header('HTTP/1.0 403 Forbidden');
//    closeConnectDB();
//    exit();
//}

//Router
switch ($request_event) {
  case 'login':
    require_once('./controller/auth.php');
    $authContoller = new authContoller();
    $authContoller->login($_POST['username'], $_POST['password']);
    break;

  case 'logout':
      require_once('./controller/auth.php');
      $authContoller = new authContoller();
      $authContoller->logout();
      break;

      case 'add_user':
          require_once('./controller/users.php');
          $usersContoller = new usersContoller();
          $usersContoller->create($_POST['USER_ID'], $_POST['NAME'], $_POST['BIRTHDAY'], $_POST['SEX'], $_POST['SMOKER'], $_POST['HAS_INJECTION'], $_POST['MORNING_START_TIME'], $_POST['MORNING_END_TIME'], $_POST['BREAKFAST_START_TIME'], $_POST['BREAKFAST_END_TIME'], $_POST['LUNCH_START_TIME'], $_POST['LUNCH_END_TIME'],
           $_POST['DINNER_START_TIME'], $_POST['DINNER_END_TIME'], $_POST['BED_START_TIME'], $_POST['BED_END_TIME'], $_POST['HEIGHT_UNIT'], $_POST['WEIGHT_UNIT'], $_POST['WAIST_UNIT']);
          break;

          case 'update_user':
              require_once('./controller/users.php');
              $usersContoller = new usersContoller();
              $usersContoller->update_user($_POST['USER_ID'], $_POST['NAME'], $_POST['BIRTHDAY'], $_POST['SEX'], $_POST['SMOKER'], $_POST['HAS_INJECTION'], $_POST['MORNING_START_TIME'], $_POST['MORNING_END_TIME'], $_POST['BREAKFAST_START_TIME'], $_POST['BREAKFAST_END_TIME'], $_POST['LUNCH_START_TIME'], $_POST['LUNCH_END_TIME'],
               $_POST['DINNER_START_TIME'], $_POST['DINNER_END_TIME'], $_POST['BED_START_TIME'], $_POST['BED_END_TIME'], $_POST['HEIGHT_UNIT'], $_POST['WEIGHT_UNIT'], $_POST['WAIST_UNIT']);
              break;

              case 'del_user':
                  require_once('./controller/users.php');
                  $usersContoller = new usersContoller();
                  $usersContoller->del_user($_POST['USER_ID']);
                  break;

          case 'get_users_list':
              require_once('./controller/users.php');
              $usersContoller = new usersContoller();
              $usersContoller->get_users_list();
              break;


              case 'get_users_info':
                  require_once('./controller/users.php');
                  $usersContoller = new usersContoller();
                  $usersContoller->get_users_info($_POST['uid']);
                  break;

                  case 'add_adminuser_firstlogin':
                      require_once('./controller/auth.php');
                      $usersContoller = new authContoller();
                      $usersContoller->add_adminuser_firstlogin($_POST['username'], $_POST['password'], $_POST['password_confirm']);
                      break;

                      case 'report_tokenCheck':
                          require_once('./controller/report.php');
                          $usersContoller = new reportContoller();
                          $usersContoller->tokenCheck($_POST['token']);
                          break;

                          case 'report_fetchReport':
                              require_once('./controller/report.php');
                              $usersContoller = new reportContoller();
                              $usersContoller->fetchReport($_POST['from_year'], $_POST['to_year'], $_POST['from_month'], $_POST['to_month']);
                              break;

                          case 'check_login':
                            require_once('./controller/auth.php');
                            $usersContoller = new authContoller();
                            $usersContoller->checkIsLogedin();
                          break;


  default:
    header('HTTP/1.0 404 Not Found');
    closeConnectDB();
    exit();
    break;
}


closeConnectDB();
unset($request_data);
unset($request_method);
unset($result);
unset($request_event);
unset($request_user_id);
