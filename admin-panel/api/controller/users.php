<?php
require_once(dirname(__FILE__, 4).'/mysql.inc.php');
session_start();
class usersContoller
{

    public function __construct()
    {
      require_once('auth.php');
      $authContoller = new authContoller();
      if (!$authContoller->isLogedin()) {
        $output = array();
        $output['success'] = "failed";
        $output['loggedin'] = "false";
        $output['msg'] = "Please login first!";
        print json_encode($output);
        exit();
      }

      $authContoller->activitynotify($_SESSION['uid']);
    }

    public function get_users_list()
    {
      $output = array();
      $sql = "SELECT ID, USER_ID, NAME FROM DM_USER;";
      $result = select_result($sql);
      while ($row1=get_row_mysqli_assoc($result)) {
        $obj = array();
        $obj['value'] = $row1['USER_ID'];
        $obj['text'] = "USER_ID:".$row1['USER_ID']." NAME:".$row1['NAME'];
        $output[] = $obj;
      }
      print json_encode($output);
    }

    public function get_users_info($uid)
    {
      $output = array();
      $sql = "SELECT * FROM DM_USER WHERE USER_ID = $uid;";
      $result = select_result($sql);
      if (get_rowCount($result) <= 0) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "Record not found!";
      }else{
        $row1=get_row_mysqli_assoc($result);
        $output['ID'] = $row1['ID'];

        $temp_userid = strval($row1['USER_ID']);
        $temp = strlen($temp_userid);
        if ($temp < 6) {
          for ($i=0; $i < 6-$temp ; $i++) {
            $temp_userid = "0" . $temp_userid;
          }
        }

        $output['USER_ID'] = $temp_userid;
        $output['NAME'] = $row1['NAME'];
        $output['BIRTHDAY'] = $row1['BIRTHDAY'];
        $output['SEX'] = $row1['SEX'];
        $output['SMOKER'] = $row1['SMOKER'];
        $output['HAS_INJECTION'] = $row1['HAS_INJECTION'];
        $output['MORNING_START_TIME'] = substr($row1['MORNING_START_TIME'], 0, 5);
        $output['MORNING_END_TIME'] = substr($row1['MORNING_END_TIME'], 0, 5);
        $output['BREAKFAST_START_TIME'] = substr($row1['BREAKFAST_START_TIME'], 0, 5);
        $output['BREAKFAST_END_TIME'] = substr($row1['BREAKFAST_END_TIME'], 0, 5);
        $output['LUNCH_START_TIME'] = substr($row1['LUNCH_START_TIME'], 0, 5);
        $output['LUNCH_END_TIME'] = substr($row1['LUNCH_END_TIME'], 0, 5);
        $output['DINNER_START_TIME'] = substr($row1['DINNER_START_TIME'], 0, 5);
        $output['DINNER_END_TIME'] = substr($row1['DINNER_END_TIME'], 0, 5);
        $output['BED_START_TIME'] = substr($row1['BED_START_TIME'], 0, 5);
        $output['BED_END_TIME'] = substr($row1['BED_END_TIME'], 0, 5);
        $output['HEIGHT_UNIT'] = $row1['HEIGHT_UNIT'];
        $output['WEIGHT_UNIT'] = $row1['WEIGHT_UNIT'];
        $output['WAIST_UNIT'] = $row1['WAIST_UNIT'];
      }
      print json_encode($output);
    }

    public function create($USER_ID, $NAME, $BIRTHDAY, $SEX, $SMOKER, $HAS_INJECTION, $MORNING_START_TIME, $MORNING_END_TIME, $BREAKFAST_START_TIME, $BREAKFAST_END_TIME, $LUNCH_START_TIME, $LUNCH_END_TIME, $DINNER_START_TIME, $DINNER_END_TIME, $BED_START_TIME, $BED_END_TIME, $HEIGHT_UNIT, $WEIGHT_UNIT, $WAIST_UNIT)
    {
      $output = array();
      if (strlen($USER_ID) != 6 || !ctype_digit($USER_ID)) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "USER_ID must be 6 digits.";
        print json_encode($output);
        exit();
      }

      if (!($this->timeformatCheck($MORNING_START_TIME)&& $this->timeformatCheck($MORNING_END_TIME)  && $this->timeformatCheck($BREAKFAST_START_TIME)  && $this->timeformatCheck($BREAKFAST_END_TIME)  && $this->timeformatCheck($LUNCH_START_TIME)  && $this->timeformatCheck($LUNCH_END_TIME)  && $this->timeformatCheck($DINNER_START_TIME)  && $this->timeformatCheck($DINNER_END_TIME)  && $this->timeformatCheck($BED_START_TIME)
      && $this->timeformatCheck($BED_END_TIME))) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "Illegal time value.";
        print json_encode($output);
        exit();
      }

      $sql = "SELECT ID FROM DM_USER WHERE USER_ID = $USER_ID;";
      $result = select_result($sql);
      if (get_rowCount($result)>0) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "USER_ID is exist.";
      }else{
        $sql = "INSERT INTO `DM_USER` (`USER_ID`, `NAME`, `BIRTHDAY`,
          `SEX`, `SMOKER`, `HAS_INJECTION`, `MORNING_START_TIME`, `MORNING_END_TIME`,
           `BREAKFAST_START_TIME`, `BREAKFAST_END_TIME`, `LUNCH_START_TIME`,
           `LUNCH_END_TIME`, `DINNER_START_TIME`, `DINNER_END_TIME`,
         `BED_START_TIME`, `BED_END_TIME`, `HEIGHT_UNIT`, `WEIGHT_UNIT`, `WAIST_UNIT`)
         VALUES ($USER_ID, '$NAME', '$BIRTHDAY',
           $SEX, $SMOKER, '$HAS_INJECTION', '$MORNING_START_TIME:00', '$MORNING_END_TIME:00',
            '$BREAKFAST_START_TIME:00', '$BREAKFAST_END_TIME:00', '$LUNCH_START_TIME:00',
            '$LUNCH_END_TIME:00', '$DINNER_START_TIME:00', '$DINNER_END_TIME:00',
          '$BED_START_TIME:00', '$BED_END_TIME:00', '$HEIGHT_UNIT', '$WEIGHT_UNIT', '$WAIST_UNIT') ;";
          try{
            $result = select_result($sql);
            $output['success'] = "true";
            $output['loggedin'] = "true";
          } catch (Exception $e){
            $output['success'] = "failed";
            $output['loggedin'] = "true";
            $output['msg'] = "$e";
          }
      }
      print json_encode($output);
    }

    public function update_user($USER_ID, $NAME, $BIRTHDAY, $SEX, $SMOKER, $HAS_INJECTION, $MORNING_START_TIME, $MORNING_END_TIME, $BREAKFAST_START_TIME, $BREAKFAST_END_TIME, $LUNCH_START_TIME, $LUNCH_END_TIME, $DINNER_START_TIME, $DINNER_END_TIME, $BED_START_TIME, $BED_END_TIME, $HEIGHT_UNIT, $WEIGHT_UNIT, $WAIST_UNIT)
    {
      $output = array();
      if (strlen($USER_ID) != 6 || !ctype_digit($USER_ID)) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "USER_ID must be 6 digits.";
        print json_encode($output);
        exit();
      }

      if (!($this->timeformatCheck($MORNING_START_TIME)&& $this->timeformatCheck($MORNING_END_TIME)  && $this->timeformatCheck($BREAKFAST_START_TIME)  && $this->timeformatCheck($BREAKFAST_END_TIME)  && $this->timeformatCheck($LUNCH_START_TIME)  && $this->timeformatCheck($LUNCH_END_TIME)  && $this->timeformatCheck($DINNER_START_TIME)  && $this->timeformatCheck($DINNER_END_TIME)  && $this->timeformatCheck($BED_START_TIME)
      && $this->timeformatCheck($BED_END_TIME))) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "Illegal time value.";
        print json_encode($output);
        exit();
      }

      $sql = "SELECT ID FROM DM_USER WHERE USER_ID = $USER_ID;";
      $result = select_result($sql);
      if (get_rowCount($result)<=0) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "Record not found.";
      }else{
        $sql = "UPDATE `DM_USER` SET `NAME` = '$NAME', `BIRTHDAY` = '$BIRTHDAY',
          `SEX` = $SEX, `SMOKER` = $SMOKER, `HAS_INJECTION` = '$HAS_INJECTION', `MORNING_START_TIME` = '$MORNING_START_TIME:00', `MORNING_END_TIME` = '$MORNING_START_TIME:00',
           `BREAKFAST_START_TIME` = '$BREAKFAST_START_TIME:00', `BREAKFAST_END_TIME` = '$BREAKFAST_END_TIME:00', `LUNCH_START_TIME` = '$LUNCH_START_TIME:00',
           `LUNCH_END_TIME` = '$LUNCH_END_TIME:00', `DINNER_START_TIME` = '$DINNER_START_TIME:00', `DINNER_END_TIME` = '$DINNER_END_TIME:00',
         `BED_START_TIME` = '$BED_START_TIME:00', `BED_END_TIME` = '$BED_END_TIME:00', `HEIGHT_UNIT` = $HEIGHT_UNIT, `WEIGHT_UNIT` = $WEIGHT_UNIT, `WAIST_UNIT` = $WAIST_UNIT WHERE `USER_ID` = $USER_ID;";
          try{
            $result = select_result($sql);
            $output['success'] = "true";
            $output['loggedin'] = "true";
          } catch (Exception $e){
            $output['success'] = "failed";
            $output['loggedin'] = "true";
            $output['msg'] = "$e";
          }
      }
      print json_encode($output);
    }

    public function timeformatCheck(&$time)
    {
      $temp = explode(":", $time);
      $hour = $temp[0];
      $minute = $temp[1];
      if ((intval($hour) >= 0 && intval($hour) <= 23) && (intval($minute) >= 0 && intval($minute) <= 59)) {
        return true;
      }
      return false;
    }

    public function del_user($USER_ID)
    {
      $output = array();
      if (strlen($USER_ID) != 6 || !ctype_digit($USER_ID)) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "USER_ID must be 6 digits.";
        print json_encode($output);
        exit();
      }

      $sql = "SELECT ID FROM DM_USER WHERE USER_ID = $USER_ID;";
      $result = select_result($sql);
      if (get_rowCount($result)<=0) {
        $output['success'] = "failed";
        $output['loggedin'] = "true";
        $output['msg'] = "Record not found.";
      }else{
        $sql = "DELETE FROM `DM_USER` WHERE `USER_ID` = $USER_ID;";
          try{
            $result = select_result($sql);
            $output['success'] = "true";
            $output['loggedin'] = "true";
          } catch (Exception $e){
            $output['success'] = "failed";
            $output['loggedin'] = "true";
            $output['msg'] = "$e";
          }
      }
      print json_encode($output);
    }
}
