<?php
require_once(dirname(__FILE__, 2).'/mysql.inc.php');
class foodAssessmentController
{
  public function __construct(&$request_user_id, &$request_method, &$data)
  {
    switch ($request_method) {

      case 'compareData':
        //echo var_dump($request_user_id);
        $compareData = $this->getCompareData($request_user_id, $data);
        print json_encode($compareData);
        break;

      case 'updateRecords':
        $this->updateRecords($request_user_id, $data);
      break;

      case 'getOldRecord':
        $oldData = $this->getOldRecord($request_user_id, $data);
        print json_encode($oldData);
        break;
 
      default:
        header('HTTP/1.0 404 Not Found');
        exit();
        break;
    }
  }
  
  public function getCompareData(&$request_user_id, &$data)
  {
    $diff_list = array();
    $dataArray = array();
    $existArray = array();
    $deleteListString = "";
    $sizeOfData = sizeof($data);
    $temp_string = "";
    if ($data != "" || $sizeOfData > 0) {
        for ($i=0; $i < $sizeOfData ; $i++) {
            $dataArray[$data[$i]->create_datetime] = $data[$i]->edit_datetime;
            $temp_string = $temp_string."'".$data[$i]->create_datetime."'";
            if ($i < sizeof($data) - 1) {
                $temp_string = $temp_string.",";
            }
        }

        $sql = 'SELECT * FROM `FOOD_SELF_ASSESS` WHERE `FOOD_SELF_ASSESS`.`create_datetime` IN (';
        $sql = $sql . $temp_string;
        $sql = $sql . ') AND `FOOD_SELF_ASSESS`.`USER_ID` = ';
        $sql = $sql . $request_user_id;
        //var_dump($sql);
        //var_dump($sql);
        $result = select_result($sql);
        if (get_rowCount($result)!=0) {
            while ($row1=get_row_mysqli_assoc($result)) {
                $existArray[] = $row1['create_datetime'];
                if ($row1['edit_datetime'] != $dataArray[$row1['create_datetime']]) {
                    $diff_list[] = $row1['create_datetime'];
                }
            }
        }

        for ($i=0; $i < $sizeOfData ; $i++) {
            if (!in_array($data[$i]->create_datetime, $existArray)) {
                $diff_list[] = $data[$i]->create_datetime;
            }
        }

        $sql = 'SELECT * FROM `FOOD_SELF_ASSESS` WHERE `FOOD_SELF_ASSESS`.`create_datetime` NOT IN (';
        $sql = $sql . $temp_string;
        $sql = $sql . ') AND `FOOD_SELF_ASSESS`.`USER_ID` = ' . $request_user_id;
        //var_dump($sql);
        //$sql = "SELECT * FROM BODY_RECORD WHERE create_datetime NOT IN ('20161223021551','20161223024850','20161223024137','20161223024406','20161224013841','20161224013841') AND USER_ID = 999999";
        $result = select_result($sql);
        if (get_rowCount($result)!=0) {
            while ($row1=get_row_mysqli_assoc($result)) {
                $deleteListString = $deleteListString."'".$row1['create_datetime']."'";
                $deleteListString = $deleteListString.",";
            }
        }

        if (strlen($deleteListString) > 3) {
          $deleteListString = substr($deleteListString, 0, strlen($deleteListString) - 1);
          $sql = 'DELETE FROM `FOOD_SELF_ASSESS` WHERE `FOOD_SELF_ASSESS`.`create_datetime` IN (';
          $sql = $sql . $deleteListString;
          $sql = $sql . ') AND `FOOD_SELF_ASSESS`.`USER_ID` = ' . $request_user_id;

          select_result($sql);
        }

    }

    unset($sizeOfData);
    unset($existArray);
    unset($dataArray);

    return $diff_list;
  }

  public function updateRecords(&$request_user_id, &$data)
  {
      //var_dump($data);
  $sizeOfData = sizeof($data);
      for ($i=0; $i < $sizeOfData ; $i++) {
          //  Columns  FOOD_ASSESS_ID USER_ID FOOD_ASSESS_DATE FOOD_RATING FOOD_QUESTION_2 FOOD_QUESTION_3a FOOD_QUESTION_3b1 FOOD_QUESTION_3b2 FOOD_QUESTION_3b3 create_datetime edit_datetime;
    $sql = "INSERT INTO `FOOD_SELF_ASSESS` (`USER_ID`, `FOOD_ASSESS_DATE`, `FOOD_RATING`,
`FOOD_QUESTION_2`,
`FOOD_QUESTION_3a`,
`FOOD_QUESTION_3b1`,
`FOOD_QUESTION_3b2`,
`FOOD_QUESTION_3b3`,
`create_datetime`, `edit_datetime`) VALUES ('$request_user_id', '".$data[$i]->FOOD_ASSESS_DATE."',
'".$data[$i]->FOOD_RATING."',
'".$data[$i]->FOOD_Q2."',
'".$data[$i]->FOOD_Q3."',
'".$data[$i]->FOOD_Q3B1."',
'".$data[$i]->FOOD_Q3B2."',
'".$data[$i]->FOOD_Q3B3."',
'".$data[$i]->create_datetime."', '".$data[$i]->edit_datetime."')
ON DUPLICATE KEY UPDATE `FOOD_ASSESS_DATE` = '".$data[$i]->FOOD_ASSESS_DATE."',
`FOOD_RATING` = '".$data[$i]->FOOD_RATING."',
`FOOD_QUESTION_2` = '".$data[$i]->FOOD_Q2."',
`FOOD_QUESTION_3a` = '".$data[$i]->FOOD_Q3."',
`FOOD_QUESTION_3b1` = '".$data[$i]->FOOD_Q3B1."',
`FOOD_QUESTION_3b2` = '".$data[$i]->FOOD_Q3B2."',
`FOOD_QUESTION_3b3` = '".$data[$i]->FOOD_Q3B3."',
`edit_datetime` = '".$data[$i]->edit_datetime."';";
    //print $sql."<br />";
    $result = select_result($sql);
      }
  }

  public function getOldRecord(&$request_user_id, &$data)
  {
    $sql = 'SELECT * FROM `FOOD_SELF_ASSESS` WHERE `FOOD_SELF_ASSESS`.`USER_ID` = '.$request_user_id;
    $result = select_result($sql);
    $output = array();
    if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
            $output[] = array(
              'USER_ID'=>$row1['USER_ID'],
              'FOOD_ASSESS_DATE'=>$row1['FOOD_ASSESS_DATE'],
              'FOOD_RATING'=>$row1['FOOD_RATING'],
              'FOOD_QUESTION_2'=>$row1['FOOD_QUESTION_2'],
              'FOOD_QUESTION_3a'=>$row1['FOOD_QUESTION_3a'],
              'FOOD_QUESTION_3b2'=>$row1['FOOD_QUESTION_3b2'],
              'FOOD_QUESTION_3b3'=>$row1['FOOD_QUESTION_3b3'],
              'create_datetime'=>$row1['create_datetime'],
              'edit_datetime'=>$row1['edit_datetime']
          );
        }
    }

    return $output;
  }
}
?>
