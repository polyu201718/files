<?php
require_once('../mysql.inc.php');
class rewardsController
{
  public function __construct(&$request_user_id, &$request_method, &$data)
  {
    switch ($request_method) {

      case 'compareData':
        //echo var_dump($request_user_id);
        $compareData = $this->getCompareData($request_user_id, $data);
        print json_encode($compareData);
        break;

        case 'insertRecords':
        $this->insertRecords($request_user_id, $data);
        break;

      case 'updateRecords':
        $this->updateRecords($request_user_id, $data);
      break;

      case 'getOldRecord':
        $oldData = $this->getOldRecord($request_user_id, $data);
        print json_encode($oldData);
        break;
 
      default:
        header('HTTP/1.0 404 Not Found');
        exit();
        break;
    }
  }
  
  public function getCompareData(&$request_user_id, &$data)
  {
    $diff_list = array();
    $dataArray = array();
    $existArray = array();
    $deleteListString = "";
    $sizeOfData = sizeof($data);
    $temp_string = "";
    if ($data != "" || $sizeOfData > 0) {
        for ($i=0; $i < $sizeOfData ; $i++) {
            $dataArray[$data[$i]->create_datetime] = $data[$i]->edit_datetime;
            $temp_string = $temp_string."'".$data[$i]->create_datetime."'";
            if ($i < sizeof($data) - 1) {
                $temp_string = $temp_string.",";
            }
        }

        $sql = 'SELECT * FROM `APP_REWARDS` WHERE `APP_REWARDS`.`create_datetime` IN (';
        $sql = $sql . $temp_string;
        $sql = $sql . ') AND `APP_REWARDS`.`USER_ID` = ';
        $sql = $sql . $request_user_id;
        //var_dump($sql);
        //var_dump($sql);
        $result = select_result($sql);
        if (get_rowCount($result)!=0) {
            while ($row1=get_row_mysqli_assoc($result)) {
                $existArray[] = $row1['create_datetime'];
                if ($row1['edit_datetime'] != $dataArray[$row1['create_datetime']]) {
                    $diff_list[] = $row1['create_datetime'];
                }
            }
        }

        for ($i=0; $i < $sizeOfData ; $i++) {
            if (!in_array($data[$i]->create_datetime, $existArray)) {
                $diff_list[] = $data[$i]->create_datetime;
            }
        }

        $sql = 'SELECT * FROM `APP_REWARDS` WHERE `APP_REWARDS`.`create_datetime` NOT IN (';
        $sql = $sql . $temp_string;
        $sql = $sql . ') AND `APP_REWARDS`.`USER_ID` = ' . $request_user_id;
        //var_dump($sql);
        //$sql = "SELECT * FROM BODY_RECORD WHERE create_datetime NOT IN ('20161223021551','20161223024850','20161223024137','20161223024406','20161224013841','20161224013841') AND USER_ID = 999999";
        $result = select_result($sql);
        if (get_rowCount($result)!=0) {
            while ($row1=get_row_mysqli_assoc($result)) {
                $deleteListString = $deleteListString."'".$row1['create_datetime']."'";
                $deleteListString = $deleteListString.",";
            }
        }

        if (strlen($deleteListString) > 3) {
          $deleteListString = substr($deleteListString, 0, strlen($deleteListString) - 1);
          $sql = 'DELETE FROM `APP_REWARDS` WHERE `APP_REWARDS`.`create_datetime` IN (';
          $sql = $sql . $deleteListString;
          $sql = $sql . ') AND `APP_REWARDS`.`USER_ID` = ' . $request_user_id;

          select_result($sql);
        }

    }

    unset($sizeOfData);
    unset($existArray);
    unset($dataArray);

    return $diff_list;
  }

  public function insertRecords(&$request_user_id, &$data)
  {
      //var_dump($data);
  $sizeOfData = sizeof($data);
      for ($i=0; $i < $sizeOfData ; $i++) { 
    $sql = "INSERT INTO `APP_REWARDS` (`USER_ID`, `REWARDS`,
    `create_datetime`, `edit_datetime`) VALUES        ('$request_user_id', '".$data[$i]->FOOD_REWARDS."',
    '".$data[$i]->create_datetime."', '".$data[$i]->edit_datetime."')";
    echo $sql."<br />";
    $result = select_result($sql);
    echo $result;
      }
  }

  public function updateRecords(&$request_user_id, &$data)
  {
      //var_dump($data);
  $sizeOfData = sizeof($data);
      for ($i=0; $i < $sizeOfData ; $i++) { 
    $sql = "UPDATE `APP_REWARDS` SET `REWARDS` = " .$data[$i]->FOOD_REWARDS ."`edit_datetime` = " . $data[$i]->edit_datetime . " WHERE `USER_ID` = '$request_user_id'";  
    //print $sql."<br />";
    $result = select_result($sql);
      }
  }

  public function getOldRecord(&$request_user_id, &$data)
  {
    $sql = "SELECT * FROM `APP_REWARDS` WHERE `APP_REWARDS`.`USER_ID` ='".$request_user_id."'";
    $result = select_result($sql);
    $output = array();
    if (get_rowCount($result)!=0) {
        while ($row1=get_row_mysqli_assoc($result)) {
            $output[] = array(
              'USER_ID'=>$row1['USER_ID'],
              'REWARDS'=>$row1['REWARDS']
              );
        }
    }

    return $output;
  }
}
?>
