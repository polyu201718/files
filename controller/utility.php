<?php
require_once(dirname(__FILE__, 2).'/mysql.inc.php');
class utilityController
{
  public function __construct(&$request_user_id, &$request_method, &$data)
  {
    switch ($request_method) {

      case 'uploadImage':
        $output = $this->uploadImage($request_user_id, $data);
        print json_encode($output);
        break;

      default:
        header('HTTP/1.0 404 Not Found');
        exit();
        break;
    }
  }

  public function uploadImage(&$request_user_id, &$data)
  {
    $output = array();
    $name = $data[0]->filename;
    $image = $data[0]->image;
    try {
      //print dirname(__FILE__, 2).'/navFood/userFoodImage/';
      print $name;
      file_put_contents(dirname(__FILE__, 2).'/navFood/userFoodImage/'.$name, base64_decode("$image") );
      $output[0]['result'] = "ok";
    } catch (Exception $e) {

      $output[0]['result'] = "failed";
      $output[0]['msg'] = $e;
    }

    return $output;
  }
}

 ?>
