<?php
require_once(dirname(__FILE__, 2).'/mysql.inc.php');
class bodyRecordContoller
{
    public function getCompareData(&$requested_user_id, &$data)
    {
        $diff_list = array();
        $dataArray = array();
        $existArray = array();
        $deleteListString = "";
        $sizeOfData = sizeof($data);
        $temp_string = "";
        if ($data != "" || $sizeOfData > 0) {
            for ($i=0; $i < $sizeOfData ; $i++) {
                $dataArray[$data[$i]->create_datetime] = $data[$i]->edit_datetime;
                $temp_string = $temp_string."'".$data[$i]->create_datetime."'";
                if ($i < sizeof($data) - 1) {
                    $temp_string = $temp_string.",";
                }
            }

            $sql = 'SELECT * FROM `BODY_RECORD` WHERE `BODY_RECORD`.`create_datetime` IN (';
            $sql = $sql . $temp_string;
            $sql = $sql . ') AND `BODY_RECORD`.`USER_ID` = ' . $requested_user_id;

            //var_dump($sql);
            $result = select_result($sql);
            if (get_rowCount($result)!=0) {
                while ($row1=get_row_mysqli_assoc($result)) {
                    $existArray[] = $row1['create_datetime'];
                    if ($row1['edit_datetime'] != $dataArray[$row1['create_datetime']]) {
                        $diff_list[] = $row1['create_datetime'];
                    }
                }
            }

            for ($i=0; $i < $sizeOfData ; $i++) {
                if (!in_array($data[$i]->create_datetime, $existArray)) {
                    $diff_list[] = $data[$i]->create_datetime;
                }
            }

            $sql = 'SELECT * FROM `BODY_RECORD` WHERE `BODY_RECORD`.`create_datetime` NOT IN (';
            $sql = $sql . $temp_string;
            $sql = $sql . ') AND `BODY_RECORD`.`USER_ID` = ' . $requested_user_id;
            //var_dump($sql);
            //$sql = "SELECT * FROM BODY_RECORD WHERE create_datetime NOT IN ('20161223021551','20161223024850','20161223024137','20161223024406','20161224013841','20161224013841') AND USER_ID = 999999";
            $result = select_result($sql);
            if (get_rowCount($result)!=0) {
                while ($row1=get_row_mysqli_assoc($result)) {
                    $deleteListString = $deleteListString."'".$row1['create_datetime']."'";
                    $deleteListString = $deleteListString.",";
                }
            }

            if (strlen($deleteListString) > 3) {
              $deleteListString = substr($deleteListString, 0, strlen($deleteListString) - 1);
              $sql = 'DELETE FROM `BODY_RECORD` WHERE `BODY_RECORD`.`create_datetime` IN (';
              $sql = $sql . $deleteListString;
              $sql = $sql . ') AND `BODY_RECORD`.`USER_ID` = ' . $requested_user_id;

              select_result($sql);
            }

        }

        unset($sizeOfData);
        unset($existArray);
        unset($dataArray);

        return $diff_list;
    }

    public function updateRecords(&$request_user_id, &$data)
    {
        //var_dump($data);
    $sizeOfData = sizeof($data);
        for ($i=0; $i < $sizeOfData ; $i++) {
            //$sql = "UPDATE `BODY_RECORD` SET (`RECORD_TYPE`=".$data[$i]->recordId.", `DATE`=".$data[$i]->date.", `TIME`=".$data[$i]->time.", `HEIGHT`=".$data[$i]->height.", `WEIGHT`=".$data[$i]->weight.", `WAIST`=".$data[$i]->waist.", `BMI`=".$data[$i]->bmi.", `BP_H`=".$data[$i]->bp_h.", `BP_L`=".$data[$i]->bp_l.", `HEART_RATE`=".$data[$i]->heart_rate.", `HbA1c`=".$data[$i]->hba1c.", `PERIOD`=".$data[$i]->period.", `TYPE_GLUCOSE`=".$data[$i]->type_glucose.", `GLUCOSE`=".$data[$i]->glucose.", `FASTING_BLOOD_SUGAR`=0, `POST_GLUCOSE`=0, `TOTAL_C`=".$data[$i]->total_c.", `LDL_C`=".$data[$i]->ldl_c.", `HDL_C`=".$data[$i]->hdl_c.", `TRIGLYCERIDES`=".$data[$i]->triglyceriders.", `REMARKS`='".$data[$i]->remarks."', `create_datetime`='".$data[$i]->create_datetime."', `edit_datetime`='".$data[$i]->edit_datetime."') WHERE `USER_ID`=".$data[$i]->userId." AND `create_datetime`='".$data[$i]->create_datetime."' IF @@ROWCOUNT=0 INSERT INTO `BODY_RECORD` VALUES (`RECORD_TYPE`=".$data[$i]->recordId.", `USER_ID`=".$request_user_id.", `DATE`=".$data[$i]->date.", `TIME`=".$data[$i]->time.", `HEIGHT`=".$data[$i]->height.", `WEIGHT`=".$data[$i]->weight.", `WAIST`=".$data[$i]->waist.", `BMI`=".$data[$i]->bmi.", `BP_H`=".$data[$i]->bp_h.", `BP_L`=".$data[$i]->bp_l.", `HEART_RATE`=".$data[$i]->heart_rate.", `HbA1c`=".$data[$i]->hba1c.", `PERIOD`=".$data[$i]->period.", `TYPE_GLUCOSE`=".$data[$i]->type_glucose.", `GLUCOSE`=".$data[$i]->glucose.", `FASTING_BLOOD_SUGAR`=0, `POST_GLUCOSE`=0, `TOTAL_C`=".$data[$i]->total_c.", `LDL_C`=".$data[$i]->ldl_c.", `HDL_C`=".$data[$i]->hdl_c.", `TRIGLYCERIDES`=".$data[$i]->triglyceriders.", `REMARKS`='".$data[$i]->remarks."', `create_datetime`='".$data[$i]->create_datetime."', `edit_datetime`='".$data[$i]->edit_datetime."')";
      $sql = "INSERT INTO `BODY_RECORD` (`USER_ID`, `RECORD_TYPE`, `DATE`,
 `TIME`,
 `HEIGHT`,
 `WEIGHT`,
 `WAIST`,
 `BMI`,
 `BP_H`,
 `BP_L`,
 `HEART_RATE`,
 `HbA1c`,
 `PERIOD`,
 `TYPE_GLUCOSE`,
 `GLUCOSE`,
 `FASTING_BLOOD_SUGAR`,
 `POST_GLUCOSE`,
 `TOTAL_C`,
 `LDL_C`,
`HDL_C`,
 `TRIGLYCERIDES`,
 `REMARKS`, `create_datetime`, `edit_datetime`) VALUES ('$request_user_id', '".$data[$i]->recordType."', '".$data[$i]->date."',
 '".$data[$i]->time."',
 '".$data[$i]->height."',
 '".$data[$i]->weight."',
 '".$data[$i]->waist."',
 '".$data[$i]->bmi."',
 '".$data[$i]->bp_h."',
 '".$data[$i]->bp_l."',
 '".$data[$i]->heart_rate."',
 '".$data[$i]->hba1c."',
 '".$data[$i]->period."',
 '".$data[$i]->type_glucose."',
 '".$data[$i]->glucose."',
 '0',
 '0',
 '".$data[$i]->total_c."',
 '".$data[$i]->ldl_c."',
 '".$data[$i]->hdl_c."',
 '".$data[$i]->triglyceriders."',
'".$data[$i]->remarks."', '".$data[$i]->create_datetime."', '".$data[$i]->edit_datetime."')
ON DUPLICATE KEY UPDATE `RECORD_TYPE` = '".$data[$i]->recordType."', `DATE` = '".$data[$i]->date."',
`TIME` = '".$data[$i]->time."',
`HEIGHT` = '".$data[$i]->height."',
`WEIGHT` = '".$data[$i]->weight."',
`WAIST` = '".$data[$i]->waist."',
`BMI` = '".$data[$i]->bmi."',
`BP_H` = '".$data[$i]->bp_h."',
`BP_L` = '".$data[$i]->bp_l."',
`HEART_RATE` = '".$data[$i]->heart_rate."',
`HbA1c` = '".$data[$i]->hba1c."',
`PERIOD` = '".$data[$i]->period."',
`TYPE_GLUCOSE` = '".$data[$i]->type_glucose."',
`GLUCOSE` = '".$data[$i]->glucose."',
`FASTING_BLOOD_SUGAR` = '0',
`POST_GLUCOSE` = '0',
`TOTAL_C` = '".$data[$i]->total_c."',
`LDL_C` = '".$data[$i]->ldl_c."',
`HDL_C` = '".$data[$i]->hdl_c."',
`TRIGLYCERIDES` = '".$data[$i]->triglyceriders."',
`REMARKS` = '".$data[$i]->remarks."', `edit_datetime` = '".$data[$i]->edit_datetime."';";
      //print $sql."<br />";
      $result = select_result($sql);
        }
    }

    public function __construct(&$request_user_id, &$request_method, &$data)
    {
        switch ($request_method) {
      case 'compareData':
        //echo var_dump($_POST);
        $compareData = $this->getCompareData($request_user_id, $data);
        print json_encode($compareData);
        break;

      case 'updateRecords':
        $this->updateRecords($request_user_id, $data);
      break;

      case 'getOldRecord':
        $oldData = $this->getOldRecord($request_user_id, $data);
        print json_encode($oldData);
        break;

      default:
        header('HTTP/1.0 404 Not Found');
        exit();
        break;
    }
    }

    public function getOldRecord(&$request_user_id, &$data)
    {
      $sql = 'SELECT * FROM `BODY_RECORD` WHERE `BODY_RECORD`.`USER_ID` = '.$request_user_id;
      $result = select_result($sql);
      $output = array();
      if (get_rowCount($result)!=0) {
          while ($row1=get_row_mysqli_assoc($result)) {
              $output[] = array('RECORD_TYPE'=>$row1['RECORD_TYPE'],
            'USER_ID'=>$row1['USER_ID'],
            'DATE'=>$row1['DATE'],
            'TIME'=>$row1['TIME'],
          'HEIGHT'=>$row1['HEIGHT'],
        'WEIGHT'=>$row1['WEIGHT'],
      'WAIST'=>$row1['WAIST'],
    'BMI'=>$row1['BMI'],
  'BP_H'=>$row1['BP_H'],
'BP_L'=>$row1['BP_L'],
'HEART_RATE'=>$row1['HEART_RATE'],
'HbA1c'=>$row1['HbA1c'],
'PERIOD'=>$row1['PERIOD'],
'TYPE_GLUCOSE'=>$row1['TYPE_GLUCOSE'],
'GLUCOSE'=>$row1['GLUCOSE'],
'FASTING_BLOOD_SUGAR'=>$row1['FASTING_BLOOD_SUGAR'],
'POST_GLUCOSE'=>$row1['POST_GLUCOSE'],
'TOTAL_C'=>$row1['TOTAL_C'],
'LDL_C'=>$row1['LDL_C'],
'HDL_C'=>$row1['HDL_C'],
'TRIGLYCERIDES'=>$row1['TRIGLYCERIDES'],
'REMARKS'=>$row1['REMARKS'],
'create_datetime'=>$row1['create_datetime'],
'edit_datetime'=>$row1['edit_datetime']);
          }
      }

      return $output;
    }
}
