<?php

// Set the database access information as constants:
DEFINE('DB_USER', 'root');
DEFINE('DB_PASSWORD', '>rZAr29)kGeg{R"m');
DEFINE('DB_HOST', 'localhost');
DEFINE('DB_NAME', 'prediabetes');


//DEFINE ('DB_USER', '13090903d');
//DEFINE ('DB_PASSWORD', 'yxrkhpmm');
//DEFINE ('DB_HOST', 'mysql.comp.polyu.edu.hk');
//DEFINE ('DB_NAME', '13090903d');

// Make the connection:
$dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) ; //or die("Unable to connect to MySQL.");


if (!$dbc) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

// Set the character set:
mysqli_set_charset($dbc, 'utf8');

function remove_mysql_danger_string($string){
  global $dbc;
  return mysqli_real_escape_string($dbc, $string);
}

// Function for close connection with Database
function closeConnectDB()
{
    global $dbc;
    mysqli_close($dbc);
}

// Function for escaping and trimming form data.
// Takes one argument: the data to be treated (string).
// Returns the treated data (string).

function escape_data($data)
{
    global $dbc;

    // Strip the slashes if Magic Quotes is on:
    if (get_magic_quotes_gpc()) {
        $data = stripslashes($data);
    }

    // Apply trim() and mysqli_real_escape_string():
    return mysqli_real_escape_string($dbc, trim($data));
} // End of the escape_data() function.

// get DB from DB
function select_result($query)
{
    global $dbc;
    $result = mysqli_query($dbc, $query) or die("Database access failed: " . mysqli_error($dbc));
    return $result;
}

// get Number of Row
function get_rowCount($result)
{
    return mysqli_num_rows($result);
}

//get row data by number (0,1,2,3,.... )
function get_row_mysqli_num($result)
{
    return mysqli_fetch_array($result, MYSQLI_NUM);
}

// get row data by col_name
function get_row_mysqli_assoc($result)
{
    return mysqli_fetch_array($result, MYSQLI_ASSOC);
}




// This function returns the hashed version of a password.
// It takes the user's password as its one argument.
// It returns a binary version of the password, already escaped to use in a query.
function get_password_hash($password)
{
    // Need the database connection:
    global $dbc;

    // Return the escaped password:
    return mysqli_real_escape_string($dbc, hash_hmac('sha256', $password, 'c#haRl891', true));
} // End of the get_password_hash() function.;
