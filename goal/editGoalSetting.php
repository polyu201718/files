<?php
    error_reporting(E_ALL);
	ini_set("display_errors", "1"); 
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');

	// Check for a form submission:
	if ($_SERVER['REQUEST_METHOD'] == 'GET') { 
		if ( isNotEmptyAndNotNull($_GET['goal_id']) && isNotEmptyAndNotNull($_GET['user_id']) &&  isNotEmptyAndNotNull($_GET['goal_type']) &&  isNotEmptyAndNotNull($_GET['goal_edit']) && isNotEmptyAndNotNull($_GET['goal_length']) && isNotEmptyAndNotNull($_GET['witness']) && isNotEmptyAndNotNull($_GET['goal_detail']) && isNotEmptyAndNotNull($_GET['other_goal'])){  
			$user = escape_data($_GET['user_id']); 
            $goalID = escape_data($_GET['goal_id']);
			$edit = escape_data($_GET['goal_edit']);
            $sql="UPDATE `USER_GOAL` SET `edit_datetime` = '$edit' WHERE `userID` = '$user' AND `goalID` = '$goalID';";
            $result = select_result($sql); 
			echo $result . ' '; 
	      
			$length = escape_data($_GET['goal_length']); 
            $sql2="UPDATE `USER_GOAL` SET `goalLength` = '$length' WHERE `userID` = '$user' AND `goalID` = '$goalID';"; 
			$result2 = select_result($sql2);
            echo $result2 . ' ' ;

			$type = escape_data($_GET['goal_type']); 
            
			$witness = escape_data($_GET['witness']);
            $sql3="UPDATE `USER_GOAL` SET `witness` = '$witness' WHERE `userID` = '$user' AND `goalID` = '$goalID';"; 
			$result3 = select_result($sql3);
            echo $result3 . ' ';

			$detail = json_decode(trim(urldecode($_GET['goal_detail'])));

			$other = json_decode(trim(urldecode($_GET['other_goal'])));
			$s = 0;
			$sql5="UPDATE `USER_GOAL` SET `goal_five` = '" . $other[$s]->goal_five . "', `goal_six` = '" . $other[$s]->goal_six . "', `goal_seven` = '" . $other[$s]->goal_seven . "', `goal_eight` = '" . $other[$s]->goal_eight . "', `goal_nine` = '" . $other[$s]->goal_nine . "', `goal_ten` = '" . $other[$s]->goal_ten . "', `goal_eleven` = '" . $other[$s]->goal_eleven . "', `goal_twelve` = '" . $other[$s]->goal_twelve . "' WHERE `userID` = '$user' AND `goalID` = '$goalID';"; 

			$result5 = select_result($sql5);

		    $sizeOfData = sizeof($detail); 
			echo $sizeOfData . ' ' ;
			for ($i=0; $i < $sizeOfData ; $i++) {
				$sql4 = "UPDATE `USER_GOAL_DETAIL` SET `type` = '" . $detail[$i]->type . "', `expectedValue` = '" . $detail[$i]->expectedValue . "', `edit_datetime` = '" . $edit . "' WHERE `goalID` = '$goalID' AND `detailID` in (SELECT did FROM (SELECT detailID as did FROM `USER_GOAL_DETAIL` WHERE `type` = '" . $detail[$i]->type . "' AND `goalID` = '$goalID') as d);";
				$result4 = select_result($sql4);
				echo $result4;  
		}
	}		
	
	closeConnectDB();
	}
?>
