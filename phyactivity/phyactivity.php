<?php
require 'vendor/autoload.php';

use GuzzleHttp\Client;

$client = new Client();

try{
    $response = $client->post('http://127.0.0.1:5000/data', [
        GuzzleHttp\RequestOptions::FORM_PARAMS => $_POST
    ]);

    http_response_code(200);
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if($e->getResponse() == null){
        http_response_code(503);
    } else {
        http_response_code($e->getResponse()->getStatusCode());
    }
}