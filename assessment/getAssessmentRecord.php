<?php
	require_once('../mysql.inc.php');
	require_once('../forms.inc.php');
	
	// Check for a form submission:
	
		if (isNotEmptyAndNotNull($_POST['user_id']) && isNotEmptyAndNotNull($_POST['assessment_week']) && isNotEmptyAndNotNull($_POST['assessment_year']) && isNotEmptyAndNotNull($_POST['start_week_date']) && isNotEmptyAndNotNull($_POST['end_week_date']) ){
			$userId = escape_data($_POST['user_id']);
			$week = escape_data($_POST['assessment_week']);
			$year = escape_data($_POST['assessment_year']);
			$startWeekDate = escape_data($_POST['start_week_date']);
			$endWeekDate = escape_data($_POST['end_week_date']);
	
			/*
			$userId = escape_data('555555');
			$year = escape_data('2016');
			$week = escape_data('14');
			$startWeekDate = escape_data('2016-3-27');
			$endWeekDate = escape_data('2016-4-2');
			*/
			
			//retrieve weight goal information
			$i=0;
			$percentage=20;
		    $sql_smoker="SELECT `USER_GOAL_DETAIL`.`expectedValue`, `USER_GOAL_DETAIL`.`unit` FROM `USER_GOAL` , `USER_GOAL_DETAIL` where `USER_GOAL`.`userID`='$userId' AND `USER_GOAL`.`goalID` = `USER_GOAL_DETAIL`.`goalID` AND `USER_GOAL_DETAIL`.`type` = 1 AND `USER_GOAL`.`goalType` = 1 AND `USER_GOAL`.`goalDate` BETWEEN '$startWeekDate' and '$endWeekDate'"  ;
		    $result_smoker = select_result($sql_smoker);
		    if (get_rowCount($result_smoker)!=0){
			 	while($row_smoker =get_row_mysqli_assoc($result_smoker)){
			 		$cls['expectedValue'] = $row_smoker['expectedValue'];
					$class['weightunit'] = $row_smoker['unit'];
			 	}
			 }
			
			//assume user will enter a weight at start of week (sql_BMI2) and end of week (sql_BMI), then compare two value difference with the expected value in goal setting
			$sql_BMI = "SELECT DATE, WEIGHT FROM BODY_RECORD WHERE RECORD_TYPE = 1 and USER_ID='$userId' AND DATE BETWEEN '$startWeekDate' AND '$endWeekDate' ORDER BY DATE DESC LIMIT 1";
			$result_BMI = select_result($sql_BMI);
 
			$sql_BMI2 = "SELECT DATE, WEIGHT FROM BODY_RECORD WHERE RECORD_TYPE = 1 and USER_ID='$userId' AND DATE BETWEEN '$startWeekDate' AND '$endWeekDate' ORDER BY DATE ASC LIMIT 1";
			$result_BMI2 = select_result($sql_BMI2);
			
			if (get_rowCount($result_smoker)!=0 && get_rowCount($result_BMI)!=0 && get_rowCount($result_BMI2)!=0){
				while($row_BMI =get_row_mysqli_assoc($result_BMI)){
					while ($row_BMI2 = get_row_mysqli_assoc($result_BMI2)){
						//this means user either has not enter a weight for start of week or end of week, incomplete
						if ($row_BMI['DATE'] == $row_BMI2['DATE']){
							$class['BMI'] = "I";
							break;
						}
						$abc = $row_BMI2['WEIGHT'] - $row_BMI['WEIGHT'];
						
						if ( $row_BMI2['WEIGHT'] - $row_BMI['WEIGHT'] >= $cls['expectedValue']){
							$class['BMI'] = "A";
							$class['WEIGHT2'] = "1";
						}else{
							$class['BMI'] = "D";
							$class['WEIGHT2'] = "1";
						}
						$class['WEIGHT2'] = "" . ($row_BMI2['WEIGHT'] - $row_BMI['WEIGHT']);

					}
					
				}
			} else {
				$class['BMI'] = "N";
				$class['WEIGHT2'] = "0";
			}

			$sql_waist="SELECT `USER_GOAL_DETAIL`.`expectedValue`, `USER_GOAL_DETAIL`.`unit` FROM `USER_GOAL` , `USER_GOAL_DETAIL` where `USER_GOAL`.`userID`='$userId' AND `USER_GOAL`.`goalID` = `USER_GOAL_DETAIL`.`goalID` AND `USER_GOAL_DETAIL`.`type` = 2 AND `USER_GOAL`.`goalType` = 1 AND `USER_GOAL`.`goalDate` BETWEEN '$startWeekDate' and '$endWeekDate'"  ;
		    $result_waist = select_result($sql_waist);
		    if (get_rowCount($result_waist)!=0){
			 	while($row_waist =get_row_mysqli_assoc($result_waist)){
			 		$cls['expectedValue2'] = $row_waist['expectedValue'];
					$class['waistunit'] = $row_waist['unit'];
			 	}
			 }
			
			//assume user will enter a weight at start of week (sql_BMI4) and end of week (sql_BMI3), then compare two value difference with the expected value in goal setting
			$sql_BMI3 = "SELECT DATE, WAIST FROM BODY_RECORD WHERE RECORD_TYPE = 1 and USER_ID='$userId' AND DATE BETWEEN '$startWeekDate' AND '$endWeekDate' ORDER BY DATE DESC LIMIT 1";
			$result_BMI3 = select_result($sql_BMI3);
 
			$sql_BMI4 = "SELECT DATE, WAIST FROM BODY_RECORD WHERE RECORD_TYPE = 1 and USER_ID='$userId' AND DATE BETWEEN '$startWeekDate' AND '$endWeekDate' ORDER BY DATE ASC LIMIT 1";
			$result_BMI4 = select_result($sql_BMI4);
			
			if (get_rowCount($result_waist)!=0 && get_rowCount($result_BMI3)!=0 && get_rowCount($result_BMI4)!=0){
				while($row_BMI3 =get_row_mysqli_assoc($result_BMI3)){
					while ($row_BMI4 = get_row_mysqli_assoc($result_BMI4)){
						//this means user either has not enter a weight for start of week or end of week, incomplete
						if ($row_BMI3['DATE'] == $row_BMI4['DATE']){
							$class['SMOKER'] = "I";
							break;
						}
						if ( $row_BMI4['WAIST'] - $row_BMI3['WAIST'] >= $cls['expectedValue2']){
							$class['SMOKER'] = "A";
						}else{
							$class['SMOKER'] = "D";
						}
						$class['WAIST2'] = "" . ($row_BMI4['WAIST'] - $row_BMI3['WAIST']);
					}
					
				}
			} else {
				$class['SMOKER'] = "N";
				$class['WAIST2'] = "0";
			}
			//Check exercise
			$sql_exe = "SELECT SUM(duration) FROM phy_activity where strength = 0 and user_id='$userId' and at between '$startWeekDate' and '$endWeekDate'";

			$sql_exe2 = "SELECT SUM(duration) as duration FROM phy_activity where strength = 1 and user_id='$userId' and at between '$startWeekDate' and '$endWeekDate'";

			$sql_exe3 = "SELECT SUM(duration) as duration FROM phy_activity where strength = 2 and user_id='$userId' and at between '$startWeekDate' and '$endWeekDate'";
			//echo $sql_exe;
			$result_exe = select_result($sql_exe);
			$result_exe2 = select_result($sql_exe2);
			$result_exe3 = select_result($sql_exe3);
			if (get_rowCount($result_exe)!=0){
				while($row_exe =get_row_mysqli_assoc($result_exe)){
					if (isset($row_exe['SUM(duration)'])) {
					$class['shortexelen'] = "" . $row_exe['SUM(duration)'];
					} 	else{
						$class['shortexelen'] = "0";
					}
				}
			}
			if (get_rowCount($result_exe2)!=0){
				while($row_exe2 =get_row_mysqli_assoc($result_exe2)){
					if (isset($row_exe2['duration'])) {
					$class['mmexelen'] = "" . $row_exe2['duration'];
				}else{
				$class['mmexelen'] = "0";
			}}
			}
			if (get_rowCount($result_exe3)!=0){
				while($row_exe3 =get_row_mysqli_assoc($result_exe3)){
						if (isset($row_exe3['duration'])) {$class['maxexelen'] = "" . $row_exe3['duration'];
						} else{
				$class['maxexelen'] = "0";
			}
				}
			}
			echo(json_encode($class));
		}
	
	
	closeConnectDB();
?>
